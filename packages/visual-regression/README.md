# visual-regression

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test visual-regression` to execute the unit tests via [Jest](https://jestjs.io).
