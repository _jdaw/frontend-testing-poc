import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface VisualRegressionProps {}

const StyledVisualRegression = styled.div`
  color: pink;
`;

export function VisualRegression(props: VisualRegressionProps) {
  return (
    <StyledVisualRegression>
      <h1>Welcome to VisualRegression!</h1>
    </StyledVisualRegression>
  );
}

export default VisualRegression;
