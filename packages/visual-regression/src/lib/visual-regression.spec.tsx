import { render } from '@testing-library/react';

import VisualRegression from './visual-regression';

describe('VisualRegression', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<VisualRegression />);
    expect(baseElement).toBeTruthy();
  });
});
