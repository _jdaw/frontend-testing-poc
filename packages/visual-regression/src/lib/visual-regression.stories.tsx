import { Story, Meta } from '@storybook/react';
import { VisualRegression, VisualRegressionProps } from './visual-regression';

export default {
  component: VisualRegression,
  title: 'VisualRegression',
} as Meta;

const Template: Story<VisualRegressionProps> = (args) => (
  <VisualRegression {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
