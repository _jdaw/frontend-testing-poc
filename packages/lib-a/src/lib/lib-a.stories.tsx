import { Story, Meta } from '@storybook/react';
import { LibA, LibAProps } from './lib-a';

export default {
  component: LibA,
  title: 'LibA',
} as Meta;

const Template: Story<LibAProps> = (args) => <LibA {...args} />;

export const Primary = Template.bind({});
Primary.args = {};
