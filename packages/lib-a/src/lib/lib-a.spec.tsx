import { render } from '@testing-library/react';

import LibA from './lib-a';

describe('LibA', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LibA />);
    expect(baseElement).toBeTruthy();
  });
});
