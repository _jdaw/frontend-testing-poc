import { render } from '@testing-library/react';

import LibB from './lib-b';

describe('LibB', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LibB />);
    expect(baseElement).toBeTruthy();
  });
});
