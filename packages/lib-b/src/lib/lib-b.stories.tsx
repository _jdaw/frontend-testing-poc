import { Story, Meta } from '@storybook/react';
import { LibB, LibBProps } from './lib-b';

export default {
  component: LibB,
  title: 'LibB',
} as Meta;

const Template: Story<LibBProps> = (args) => <LibB {...args} />;

export const Primary = Template.bind({});
Primary.args = {};
